require('dotenv').config();
const fs = require('fs-extra');
const chokidar = require('chokidar');
const path = require('path');

const dirs = {
	BP: 'development_behavior_packs',
	RP: 'development_resource_packs'
};

(async () => {
	for (const dir of Object.keys(dirs)) {
		let recentChange = false;

		const onChange = async () => {
			const dest = path.join(process.env.DEST, dirs[dir], dir);
			await fs.remove(dest)
			await fs.copy(path.join('main', dir), dest);
		};

		onChange();

		chokidar.watch(path.join('main', dir)).on('all', () => {
			if (!recentChange) {
				recentChange = true;

				setTimeout(() => {
					recentChange = false;

					onChange();
				}, 500);
			}
		});
	}
})();